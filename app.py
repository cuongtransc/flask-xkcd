#!/usr/bin/env python

from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    'https://imgs.xkcd.com/comics/lightning_distance.png',
    'https://imgs.xkcd.com/comics/standards.png',
    'https://imgs.xkcd.com/comics/python.png',
    'https://imgs.xkcd.com/comics/compiling.png',
    'https://imgs.xkcd.com/comics/random_number.png',
    'https://imgs.xkcd.com/comics/im_so_random.png',
    'https://imgs.xkcd.com/comics/ayn_random.png',
    'https://imgs.xkcd.com/comics/cant_sleep.png',
    'https://imgs.xkcd.com/comics/far_away.png',
]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")

